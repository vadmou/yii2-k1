<?php
namespace app\controllers;


class MyController extends AppController
{

    /**
     * @return Action
     */
    public function actionIndex($id = null)
    {
        $hi = 'Hello World!';
        $names = ['Nikolay', 'Alexey', 'Dmitriy'];

        if(!$id) $id = 'test';

//        return $this->render ('index', ['hello' => $hi, 'names' => $names]);
        return $this->render ('index', compact('hi', 'names', 'id'));
    }
}