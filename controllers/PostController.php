<?php

namespace app\controllers;

use app\models\Category;
use Yii;
use app\models\TestForm;

class PostController extends AppController
{

    public $layout = 'basic';

    public function actionIndex()
    {
        if (Yii::$app->request->isAjax) {
//            $this->debug($_GET);
            Yii::$app->request->post();
            return 'test';
        }

//        $post = TestForm::findOne(1);
//        $post->email = 'test@test.com';
//        $post->save();
//        $post->delete();


        $model = new TestForm();

//        $model->name = 'Имя модели';
//        $model->email = 'email@email.com';
//        $model->text = 'Текст модели';
//        $model->save();


        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Данные приняты');
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка предачи данных');
            }
        }

        return $this->render('index', compact('model'));
    }

    public function actionShow()
    {
        $this->view->title = 'Одна статья';
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => 'ключевые слова']);
        $this->view->registerMetaTag(['name' => 'description', 'content' => 'описание страницы']);

//        $cats = Category::find()->all();
//        $cats = Category::find()->orderBy(['id' => SORT_DESC])->all();  // сотрировка id по убыванию
//        $cats = Category::find()->asArray()->all();  // достаем данные массивом (потребляет меньше памяти, меньше запроосов)
//            $cats ['title'];  // в этом случае так обращаться к данным массива
//        $cats = Category::find()->asArray()->where('parent=691')->all();  // выбираем все данные parent со знач. 691
//        $cats = Category::find()->asArray()->where(['parent' => 691])->all();  // выбираем все данные parent со знач. 691
//        $cats = Category::find()->asArray()->where('parent=691')->limit(1)->all();  // выбираем один parent со знач. 691
//        $cats = Category::find()->asArray()->where('parent=691')->limit(1)->one();  // выбираем один parent со знач. 691, при этом формируется одномерный массив
//        $cats = Category::find()->asArray()->where(['like', 'title', 'pp'])->all();  // выбираем из title все ячейки с буквами "pp"
//        $cats = Category::find()->asArray()->where(['<=', 'id', 695])->all();  // выбираем из id все ячейки до 695 включительно
//        $cats = Category::find()->asArray()->where(['<=', 'id', 695])->count();  // выводит кол-во ячеек с id до 695 включительно
//        $cats = Category::findOne(['parent' => 691]);  // выводит один объект со значением parent = 691
//        $cats = Category::findAll(['parent' => 691]);  // выводит все объекты со значением parent = 691

//        $query = "SELECT * FROM categories WHERE title LIKE '%pp%' ";   // SQL запрос (опасен, возможны SQL инъекции)
//        $cats = Category::findBySql($query)->all();

//        $query = "SELECT * FROM categories WHERE title LIKE :search";   // SQL запрос - безопасен
//        $cats = Category::findBySql($query, [':search' => '%pp%'])->all();

        return $this->render('show', compact('cats'));
    }
}