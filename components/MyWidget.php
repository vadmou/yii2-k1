<?php

namespace app\components;

use yii\base\Widget;

class MyWidget extends Widget {

    public $name;


    public function init()
    {
        parent::init();
//        if ( $this->name === NULL) $this->name = "гость";

        ob_start();  // начало буферизации (все, что начинается с begin() во view)
    }

    public function run(){
        $content = ob_get_clean();  // помещаем все данные буфера в переменную content
        $content = mb_strtoupper($content, 'utf-8');

        return $this->render('my', compact('content'));

//        return "<h1>Привет {$this->name}</h1>";
    }

}